
package com.esotericsoftware.spine;

public interface Updatable {
    void update();
}
