package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;

import java.util.ArrayList;

/**
 * @author hubert
 *         Main scene for the stand-alone activiy that shows purchasable skins
 */
class PreviewSkinScene extends Stage implements InputProcessor {
    private final String ATLAS_FILENAME = "skeleton.atlas";
    private final String SKELETON_FILENAME = "skeleton.json";
    private Button okButton;
    private Button backButton;
    private Button lockedButton;
    private Image premiumBanner;
    private ArrayList<SceneTemplate> knownScenes;
    private int currentSceneIndex = 0;
    private PolygonSpriteBatch batch = new PolygonSpriteBatch();
    private Vector2 lastPointerPosition;
    private AssetManager assetManager = new AssetManager();
    private Image throbber;
    private boolean throbberDisplayed = true;

    PreviewSkinScene(final ArrayList<SceneTemplate> knownScenes) {
        Gdx.input.setInputProcessor(this);
        this.knownScenes = knownScenes;

        displayThrobber();

        loadAssets();


    }

    private void displayThrobber() {
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        throbber = new Image(new Texture("gear.png"));
        throbber.setOrigin(
                throbber.getWidth() / 2f,
                throbber.getHeight() / 2f
        );
        throbber.setPosition(
                width / 2 - throbber.getWidth() / 2f,
                height / 2 - throbber.getHeight() / 2f
        );
        addActor(throbber);
        throbber.addAction(Actions.forever(Actions.rotateBy(360, 5, Interpolation.linear)));
        throbberDisplayed = true;
    }

    private void hideThrobber() {
        throbber.remove();
        throbberDisplayed = false;
    }

    private void onLoadingFinished() {
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();
        hideThrobber();
        float characterScale = .5f;
        float buddyScale = .65f * characterScale;
        float horizonHeight = height / 3;


        //Borders
        Image borderLeft = new Image(assetManager.get("border.png", Texture.class));
        float borderScaleY = height / borderLeft.getHeight();
        borderLeft.setScaleX(-1);
        borderLeft.setScaleY(borderScaleY);

        Image borderRight = new Image(assetManager.get("border.png", Texture.class));
        borderRight.setX(knownScenes.size() * width);
        borderRight.setScaleY(borderScaleY);
        addActor(borderLeft);
        addActor(borderRight);

        //Init scenes
        for (int i = 0; i < knownScenes.size(); i++) {
            Group currentScene = new Group();

            //Init BG
            SceneTemplate template = knownScenes.get(i);
            Image background = new Image(assetManager.get(template.getBackgoundName(), Texture.class));
            float backgroundScale = width / background.getWidth();

            background.setPosition(0, 0);
            background.setScale(backgroundScale);
            currentScene.addActor(background);

            //init buddies
            for (int j = 0; j < knownScenes.get(i).getBuddiesNames().size(); j++) {
                float buddyOffset = width / (knownScenes.get(i).getBuddiesNames().size() + 1);
                float x = (j + 1) * buddyOffset;
                PassiveAnimatedCharacter buddy = new PassiveAnimatedCharacter(buddyScale, SKELETON_FILENAME, assetManager.get(ATLAS_FILENAME, TextureAtlas.class));
                buddy.setPosition(x, horizonHeight);
                buddy.setSkinByName(knownScenes.get(i).getBuddiesNames().get(j));
                buddy.setMovingProbability(0);
                currentScene.addActor(buddy);
            }

            //init hero
            AnimatedCharacter hero = new AnimatedCharacter(characterScale, "skeleton.json", assetManager.get(ATLAS_FILENAME, TextureAtlas.class));
            hero.setPosition(width / 2, horizonHeight);
            hero.setSkinByName(knownScenes.get(i).getHeroName());
            currentScene.addActor(hero);

            currentScene.setPosition(width * i, 0);
            addActor(currentScene);
        }

        //Init buttons

        okButton = new Button(new SpriteDrawable(new Sprite(assetManager.get("ok.png", Texture.class))));
        okButton.setPosition(width / 2 - okButton.getWidth() / 2, -okButton.getHeight());
        okButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                String sceneName = knownScenes.get(currentSceneIndex).getName();
                PreferenceManager.getInstance().setChosenScene(sceneName);
                PreviewSkinScene.this.showScene(currentSceneIndex, false);
            }
        });
        addActor(okButton);


        backButton = new Button(new SpriteDrawable(new Sprite(assetManager.get("back.png", Texture.class))));
        backButton.setPosition(width / 2 - backButton.getWidth() / 2, -backButton.getHeight());
        addActor(backButton);
        backButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                Gdx.app.exit();
            }
        });

        lockedButton = new Button(new SpriteDrawable(new Sprite(assetManager.get("locked.png", Texture.class))));
        lockedButton.setPosition(width / 2 - lockedButton.getWidth() / 2, -lockedButton.getHeight());
        addActor(lockedButton);
        lockedButton.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {

            }
        });


        //Calculating scale for the buttons
        float percentageOfScreen = 25f;
        Texture tmp = new Texture(Gdx.files.internal("ok.png"));//Assumes all buttons have the same size.
        float BUTTON_SCALE = width / tmp.getWidth();
        BUTTON_SCALE *= percentageOfScreen / 100f;
        okButton.setSize(okButton.getWidth() * BUTTON_SCALE, okButton.getHeight() * BUTTON_SCALE);
        backButton.setSize(backButton.getWidth() * BUTTON_SCALE, backButton.getHeight() * BUTTON_SCALE);
        lockedButton.setSize(lockedButton.getWidth() * BUTTON_SCALE, lockedButton.getHeight() * BUTTON_SCALE);

        //Premium banner
        premiumBanner = new Image(assetManager.get("premium.png", Texture.class));
        percentageOfScreen = 80f;
        float BANNER_SCALE = width / premiumBanner.getWidth();
        BANNER_SCALE *= percentageOfScreen / 100f;
        premiumBanner.setScale(BANNER_SCALE);
        premiumBanner.setPosition(getWidth(), getHeight());
        addActor(premiumBanner);


        showScene(0, false);
    }

    private void loadAssets() {
        assetManager.load(ATLAS_FILENAME, TextureAtlas.class);
        assetManager.load("border.png", Texture.class);
        assetManager.load("ok.png", Texture.class);
        assetManager.load("back.png", Texture.class);
        assetManager.load("locked.png", Texture.class);
        assetManager.load("premium.png", Texture.class);
        for (SceneTemplate template : knownScenes) {
            assetManager.load(template.getBackgoundName(), Texture.class);
        }
        long startTime = System.nanoTime();
//        assetManager.finishLoading();
        long endTime = System.nanoTime();
        Gdx.app.log("TIMING", "Loading assets took " + (endTime - startTime) / 1000000 + "ms");
    }

    private void addAssets(SceneTemplate template) {
        assetManager.load(template.getBackgoundName(), Image.class);
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        lastPointerPosition = new Vector2(screenX, screenY);
        return super.touchDown(screenX, screenY, pointer, button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        Vector2 pos = new Vector2(screenX, screenY);
        pos.sub(lastPointerPosition);
        if (pos.len() > 200f) {
            if (pos.x > 0) {
                showNextScene();
            } else {
                showPreviousScene();
            }
            return true;
        } else {
            return super.touchUp(screenX, screenY, pointer, button);
        }
    }

    private void showPreviousScene() {
        currentSceneIndex++;
        boolean bounce = false;
        if (currentSceneIndex >= knownScenes.size()) {
            currentSceneIndex = 0;
            bounce = true;
        }
        showScene(currentSceneIndex, bounce);
    }

    private void showNextScene() {
        currentSceneIndex--;
        boolean bounce = false;
        if (currentSceneIndex < 0) {
            currentSceneIndex = knownScenes.size() - 1;
            bounce = true;
        }
        showScene(currentSceneIndex, bounce);
    }

    private void showScene(int sceneIndex, boolean bounce) {
        float x = sceneIndex * getWidth();
        float TRANSITION_DURATION = 1.2f;
        Interpolation i = (bounce) ? new Interpolation.Swing(1) : new Interpolation.PowOut(5);
        addAction(Actions.moveTo(-x, 0, TRANSITION_DURATION, i));
        Button buttonToShow;
        if (knownScenes.get(sceneIndex).getName().equals(PreferenceManager.getInstance().getChosenScene())) {
            buttonToShow = backButton;
        } else {
            if (!knownScenes.get(sceneIndex).isFree() && !PreferenceManager.getInstance().isPremium()) {
                buttonToShow = lockedButton;
                premiumBanner.clearActions();
                premiumBanner.setPosition((getWidth() * sceneIndex + getWidth() / 2) - premiumBanner.getWidth() * premiumBanner.getScaleX() / 2, getHeight());
                premiumBanner.addAction(Actions.moveTo((getWidth() * sceneIndex + getWidth() / 2) - premiumBanner.getWidth() * premiumBanner.getScaleX() / 2,
                        getHeight() - (premiumBanner.getHeight() * premiumBanner.getScaleY()), TRANSITION_DURATION, Interpolation.bounceOut));
            } else {
                buttonToShow = okButton;
            }
        }
        okButton.addAction(Actions.moveTo(okButton.getX(), -okButton.getHeight(), TRANSITION_DURATION / 2, new Interpolation.PowOut(5)));
        backButton.addAction(Actions.moveTo(backButton.getX(), -backButton.getHeight(), TRANSITION_DURATION / 2, new Interpolation.PowOut(5)));
        lockedButton.addAction(Actions.moveTo(lockedButton.getX(), -lockedButton.getHeight(), TRANSITION_DURATION / 2, new Interpolation.PowOut(5)));

        buttonToShow.clearActions();
        buttonToShow.addAction(Actions.sequence(
                Actions.delay(TRANSITION_DURATION / 2),
                Actions.moveTo((getWidth() * sceneIndex + getWidth() / 2) - buttonToShow.getWidth() / 2, -buttonToShow.getHeight(), 0),
                Actions.moveTo((getWidth() * sceneIndex + getWidth() / 2) - buttonToShow.getWidth() / 2, 0, TRANSITION_DURATION, new Interpolation.PowOut(5))));
    }

    @Override
    public void dispose() {
        batch.dispose();
        assetManager.dispose();
    }

    @Override
    public void draw() {
        batch.setProjectionMatrix(getCamera().combined);
        batch.begin();
        getRoot().draw(batch, 1);
        batch.end();
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if (assetManager.update((int) delta / 50)
                && (throbberDisplayed)) {
            onLoadingFinished();
        }
    }
}
