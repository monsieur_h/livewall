package com.monsieur_h.livewall;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Timer.Task;

public class FallingTask extends Task{

	private Vector3 direction;
	private AnimatedCharacter character;
	
	public FallingTask(AnimatedCharacter target, Vector3 direction) {
		super();
		this.direction = direction;
		this.character = target;
	}
	@Override
	public void run() {
		this.character.onTilt(direction);
	}

}
