package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.esotericsoftware.spine.*;
import com.esotericsoftware.spine.AnimationState.AnimationStateListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author hubert
 *         A character living in a scene and reacting to events. Based on Spine skeleton for animation/display
 */
@SuppressWarnings("FieldCanBeLocal")
class AnimatedCharacter extends Actor implements Comparable<AnimatedCharacter>, AnimationStateListener, Disposable {
    final String IDLE_ANIMATION_NAME = "breathe";
    final int MAX_IDLE_LOOP = 2;
    private final String ON_SWIPE_ANIMATION_NAME = "onSwipe";
    private final String ON_SWIPE_UP_ANIMATION_NAME = "onSwipeUp";
    private final String ON_TAP_ON_ANIMATION_NAME = "onTapOn";
    private final String ON_TAP_OUT_ANIMATION_NAME = "onTapOut";
    private final String ON_TILT_ANIMATION_NAME = "onTilt";
    private final float DEFAULT_TRANSITION_TIME = .05f;
    private final float MAX_TILT_TRESHOLD = 20;//Percentage of max tilt required to trigger tilt animation
    protected Skeleton skeleton;
    protected Vector2 position;
    AnimationState state;
    private float CHARACTER_RADIUS = 250f;
    private SkeletonMeshRenderer renderer;
    private SkeletonData skelData;
    private Vector3 maxAcceleration;
    private Array<Skin> availableSkins;
    private List<String> idleAnimationList;
    private List<String> unInterruptibleAnimationList;
    private SkeletonJson json;

    private String onTiltAnimation;

    //=================================================
    // Ctors and getters/setters
    //=================================================

    AnimatedCharacter(float scale, String skeletonFileName, TextureAtlas atlas) {
        this.init(scale, skeletonFileName, atlas);
    }

    protected void init(float scale, String skeletonFileName, TextureAtlas atlas) {
        this.renderer = new SkeletonMeshRenderer();
        this.json = new SkeletonJson(atlas);
        json.setScale(scale);
        this.skelData = json.readSkeletonData(Gdx.files.internal(skeletonFileName));

        this.skeleton = new Skeleton(skelData);
        AnimationStateData stateData = new AnimationStateData(skelData);
        this.state = new AnimationState(stateData);

        this.setWidth(skelData.getWidth() * scale);
        this.setHeight(skelData.getHeight() * scale);

        this.CHARACTER_RADIUS *= scale;

        this.maxAcceleration = new Vector3(0, 0, 0);

        this.state.addListener(this);

        this.onTiltAnimation = ON_TILT_ANIMATION_NAME;

        this.initAnimation(skelData);

        this.setTouchable(Touchable.enabled);

        this.initSkins();
    }

    void setSkinByName(String skinName) {
        for (Skin skin : availableSkins) {
            if (skin.getName().equals(skinName)) {
                this.skeleton.setSkin(skin);
                this.skeleton.setSlotsToSetupPose();
                return;
            }
        }
    }

    public void setSkin(Skin newSkin) {
        this.skeleton.setSkin(newSkin);
        this.skeleton.setSlotsToSetupPose();
    }

    private void initSkins() {
        this.availableSkins = this.skelData.getSkins();
        this.skeleton.setSkin(skelData.getDefaultSkin());
    }


    /**
     * Reads all animations from the skeleton.json file and stores them according to their names.
     * Animation beginning whith "idle..." will be triggered when the character is idle, animation
     * containing the word "important" won't be interrupted, whatever happens.
     *
     * @param skelData the {@link SkeletonData} element containing the infos read from the file
     */
    private void initAnimation(SkeletonData skelData) {
        this.idleAnimationList = new ArrayList<String>();
        this.unInterruptibleAnimationList = new ArrayList<String>();

        Array<Animation> allAnims = skelData.getAnimations();
        for (Animation animation : allAnims) {
            if (animation.getName().matches("^idle.*")) {
                this.idleAnimationList.add(animation.getName());
            }
            if (animation.getName().matches("important")) {
                this.unInterruptibleAnimationList.add(animation.getName());
            }
        }
        this.unInterruptibleAnimationList.add(ON_TILT_ANIMATION_NAME);
        this.setDefaultMix(skelData);
        this.state.setAnimation(0, IDLE_ANIMATION_NAME, true);
    }


    /**
     * Inits transitions between all animations with a default value DEFAULT_TRANSITION_TIME
     *
     * @param skelData the skeleton data to get animations
     */
    private void setDefaultMix(SkeletonData skelData) {
        for (int i = 0; i < skelData.getAnimations().size; i++) {
            for (int j = 0; j < skelData.getAnimations().size; j++) {
                state.getData().setMix(skelData.getAnimations().get(i), skelData.getAnimations().get(j), DEFAULT_TRANSITION_TIME);
            }
        }
    }

    /**
     * Tests if the character is currently in a state where it can be interrupted.
     *
     * @return true if its running an non-important animation
     */
    private boolean isInterruptible() {
        String currentAnimationName = this.state.getCurrent(0).getAnimation().getName();
        return !this.unInterruptibleAnimationList.contains(currentAnimationName);
    }

    /**
     * Randomly picks a new idle animation from the list of idle animations.
     */
    protected void setNewIdleAnimation() {
        if (!this.isInterruptible())
            return;

        Random r = new Random();
        int index = r.nextInt(this.idleAnimationList.size());
        this.state.setAnimation(0, this.idleAnimationList.get(index), false);
        this.state.addAnimation(0, IDLE_ANIMATION_NAME, true, 0);
    }

    //=================================================
    //Framework specific functions
    //=================================================

    /**
     * Updates the status of the character based on delta time
     *
     * @param delta the time passed since the previous frame
     */
    public void act(float delta) {
        super.act(delta);
        this.state.update(delta);
        this.state.apply(this.skeleton);
        this.skeleton.updateWorldTransform();
    }

    /**
     * Draws the character using the {@link Batch}
     *
     * @param batch       the {@link Batch} used to draw the current frame
     * @param parentAlpha the alpha used to mix [unused]
     */
    public void draw(Batch batch, float parentAlpha) {
        PolygonSpriteBatch b = (PolygonSpriteBatch) batch; //FIXME : Works with a cast, not with PolygonSpriteBatch as a parameter : why ?!
        this.renderer.draw(b, this.skeleton);
    }


    /**
     * Simply sets the position in absolute world coordinates
     *
     * @param x the horizontal coord starting left
     * @param y the vertical coord starting bottom
     */
    @Override
    public void setPosition(float x, float y) {
        super.setPosition(x, y);
        this.skeleton.setPosition(x, y);
        this.position = new Vector2(x, y);
    }

    public Vector2 getPosition() {
        return this.position;
    }

    //=================================================
    //Function implemented from AnimationStateListener
    //=================================================
    @Override
    public void event(int trackIndex, Event event) {
    }

    /**
     * Triggered on the completion of any animation. Used to choose a new idle animation from the list.
     *
     * @param trackIndex the track index of the animation
     * @param loopCount  in case of looping animation, the count of the loop
     */
    @Override
    public void complete(int trackIndex, int loopCount) {
        if (loopCount > this.MAX_IDLE_LOOP) {
            this.skeleton.setFlipX(false);
            this.setNewIdleAnimation();
        }
    }

    /**
     * Triggered at the beginning of any animation. Mainly used for logging purposes
     *
     * @param trackIndex the index number of the started animation
     */
    @Override
    public void start(int trackIndex) {

    }

    @Override
    public void end(int trackIndex) {

    }

    /**
     * Triggered when a swipe gesture is detected
     *
     * @param direction Vector2 holding the normalized vector representing
     *                  the direction of the gesture
     */
    void onSwipe(Vector2 direction) {
        if (PreferenceManager.getInstance().isSwipable() && isInterruptible()) {
            if (Math.abs(direction.x) > Math.abs(direction.y)) {
                this.state.setAnimation(0, ON_SWIPE_ANIMATION_NAME, true);
                if (direction.x < 0) {
                    this.skeleton.setFlipX(false);
                } else {
                    this.skeleton.setFlipX(true);
                }
            } else {
                this.state.setAnimation(0, ON_SWIPE_UP_ANIMATION_NAME, false);
            }
            this.state.addAnimation(0, IDLE_ANIMATION_NAME, true, 0);
        }
    }

    public void onAccelerate(Vector3 acceleration, float delta) {
        Vector3 accelerationPerSecond = acceleration.scl(1 / delta);
        this.updateMaximumAcceleration(accelerationPerSecond);

        accelerationPerSecond.x = Math.abs(accelerationPerSecond.x);
        accelerationPerSecond.y = Math.abs(accelerationPerSecond.y);
        accelerationPerSecond.z = Math.abs(accelerationPerSecond.z);

        Vector3 relativeAcceleration = new Vector3();
        relativeAcceleration.x = accelerationPerSecond.x / this.maxAcceleration.x;
        relativeAcceleration.y = accelerationPerSecond.y / this.maxAcceleration.y;
        relativeAcceleration.z = accelerationPerSecond.z / this.maxAcceleration.z;

        if (relativeAcceleration.x > MAX_TILT_TRESHOLD / 100
                || relativeAcceleration.y > MAX_TILT_TRESHOLD / 100
                || relativeAcceleration.z > MAX_TILT_TRESHOLD / 100) {
            this.onTilt(acceleration.nor());
        }


    }


    private void updateMaximumAcceleration(Vector3 acceleration) {
        this.maxAcceleration.x = (this.maxAcceleration.x < Math.abs(acceleration.x)) ? Math.abs(acceleration.x) : this.maxAcceleration.x;
        this.maxAcceleration.y = (this.maxAcceleration.y < Math.abs(acceleration.y)) ? Math.abs(acceleration.y) : this.maxAcceleration.y;
        this.maxAcceleration.z = (this.maxAcceleration.z < Math.abs(acceleration.z)) ? Math.abs(acceleration.z) : this.maxAcceleration.z;
    }


    void onTapOn() {
        if (PreferenceManager.getInstance().isTapable() && this.isInterruptible()) {
            this.state.setAnimation(0, ON_TAP_ON_ANIMATION_NAME, true);
            this.state.addAnimation(0, IDLE_ANIMATION_NAME, true, 0);
        }
    }

    private void onTapOut(Vector2 position) {
        if (PreferenceManager.getInstance().isTapable() && this.isInterruptible()) {
            if (position.x > this.position.x) {
                this.skeleton.setFlipX(true);
            } else {
                this.skeleton.setFlipX(false);
            }
            this.state.setAnimation(0, ON_TAP_OUT_ANIMATION_NAME, true);
            this.state.addAnimation(0, IDLE_ANIMATION_NAME, true, 0);
        }
    }

    void onTapOut(float x, float y) {
        Vector2 tmp = new Vector2(x, y);
        this.onTapOut(tmp);
    }

    void onTilt(Vector3 direction) {
        if (PreferenceManager.getInstance().isTiltable() && this.isInterruptible()) {
            if (direction.x < 0) {
                this.skeleton.setFlipX(true);
            } else {
                this.skeleton.setFlipX(false);
            }
            this.state.setAnimation(0, this.onTiltAnimation, false);
            this.state.addAnimation(0, IDLE_ANIMATION_NAME, true, 0);
        }
    }

    public boolean contains(Vector2 point) {
        return this.isInside(point);
    }

    boolean contains(float x, float y) {
        Vector2 vec = new Vector2(x, y);
        return this.isInside(vec);
    }

    private boolean isInside(Vector2 point) {
        //FIXME : Coordinates are differents between screenspace and worldspace

        return this.position.dst(point) <= this.CHARACTER_RADIUS;
    }

    public float getX() {
        return this.skeleton.getX();
    }

    public float getY() {
        return this.skeleton.getY();
    }

    public float getScale() {
        return this.json.getScale();
    }

    @Override
    public int compareTo(AnimatedCharacter other) {
        if (this.getScale() < other.getScale()) {
            return -1;
        } else if (this.getScale() > other.getScale()) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void dispose() {
    }
}
