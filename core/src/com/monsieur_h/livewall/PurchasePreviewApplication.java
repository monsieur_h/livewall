package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * @author hubert
 * Application used as a stand-alone activity. Shows the different skins avalaible for purchase.
 */
public class PurchasePreviewApplication extends Application {
	PreviewSkinScene stage;
	
	@Override
	public void create () {
		super.create();
		long startTime = TimeUtils.millis();
		Gdx.app.log("CHRONO", "Starting loading...");
		this.stage = new PreviewSkinScene(this.knownSceneTemplates);
		long elapsedTime = TimeUtils.timeSinceMillis(startTime);
		Gdx.app.log("CHRONO", "Done loading in " + elapsedTime);
	}

	@Override
	public void render () {
		stage.act(Gdx.graphics.getDeltaTime());
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.draw();
	}
	
	@Override
	public void dispose(){
		stage.dispose();
	}
	
	protected void previewStateChange(boolean isPreview){
	}

	@Override
	public void resize(int width, int height) {
		Gdx.app.log("RESIZE", "W:" + width + " H:" + height);
	}
}
