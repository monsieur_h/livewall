package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector3;

/**
 * Wrapper for the accelerometer
 * Created by hubert on 19/07/15.
 */
public class Accelerometer {
    private Vector3 acceleration;
    private float maxAcceleration;
    private float lastDelta;

    public Accelerometer() {
        acceleration = new Vector3();
        Vector3 maxAcc = new Vector3(20, 20, 20); // LibGdx accelerometer is from 10 to -10 (Doesn't seem correct though)
        maxAcceleration = maxAcc.len();
        if (!Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer)) {
            Gdx.app.log("ERROR", "Accelerometer not available");
        }
    }

    public void update(float delta) {
        lastDelta = delta;
        readAcceleration();
    }

    public Vector3 getCurrentAcceleration() {
        return acceleration.cpy().scl(lastDelta);
    }

    public Vector3 getDirection() {
        return acceleration.cpy().nor();
    }

    /**
     * Uses the max sensor (10, -10) from the accelerator, and computes the relative force of the movement
     *
     * @return the strenght of the current movement on a 0, 1 scale
     */
    public float getAccelerationStrength() {
        float currentAcceleration = acceleration.len();
        if (currentAcceleration > maxAcceleration) {
            maxAcceleration = currentAcceleration; //Adapt max to sensors, since different phones have different scales
        }
        return (currentAcceleration / maxAcceleration);
    }

    private void readAcceleration() {
        acceleration.x = Gdx.input.getAccelerometerX();
        acceleration.y = Gdx.input.getAccelerometerY();
        acceleration.z = Gdx.input.getAccelerometerZ();
    }
}
