package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import java.util.Map;

/**
 * Manages the preferences application-wide
 * Created by hubert on 31/07/15.
 */
public class PreferenceManager {
    private static final String DEFAULT_SCENE_NAME = "default";
    private static PreferenceManager instance;
    private final Preferences settings;
    private boolean tapable = false;
    private boolean swipable = false;
    private boolean tiltable = false;
    private boolean premium = false;
    private String chosenScene;

    private PreferenceManager() {
        settings = Gdx.app.getPreferences("com.monsieur_h.livewall");
        readFromFileSystem();
        logContent();
    }

    public static PreferenceManager getInstance() {
        if (instance == null) {
            instance = new PreferenceManager();
        }
        return instance;
    }

    private void logContent() {
        Map<String, ?> map = settings.get();
        Gdx.app.log("PREF_MANAGER", "Read from preferences : " + map.toString());
        Gdx.app.log("PREF_MANAGER", "Setting up tilt : " + this.tiltable);
        Gdx.app.log("PREF_MANAGER", "Setting up tap : " + this.tapable);
        Gdx.app.log("PREF_MANAGER", "Setting up swipe : " + this.swipable);
        Gdx.app.log("PREF_MANAGER", "Premium pass : " + this.premium);
        Gdx.app.log("PREF_MANAGER", "Scene : " + this.chosenScene);
    }

    private void readFromFileSystem() {
        this.tapable = settings.getBoolean("tap_reaction_checkbox", true);
        this.swipable = settings.getBoolean("swipe_reaction_checkbox", true);
        this.tiltable = settings.getBoolean("tilt_reaction_checkbox", true);
        this.premium = settings.getBoolean("premium", false);
        this.chosenScene = settings.getString("chosenScene", DEFAULT_SCENE_NAME);
    }

    public void reload() {
        readFromFileSystem();
    }

    public boolean isTapable() {
        return tapable;
    }

    public boolean isSwipable() {
        return swipable;
    }

    public boolean isTiltable() {
        return tiltable;
    }

    public boolean isPremium() {
        return premium;
    }

    public String getChosenScene() {
        return chosenScene;
    }

    public void setChosenScene(String chosenScene) {
        this.chosenScene = chosenScene;
        writeToFile();
    }

    /**
     * Writes all changes to the filesystem
     */
    private void writeToFile() {
        settings.putString("chosenScene", getChosenScene());
        settings.putBoolean("tap_reaction_checkbox", isTapable());
        settings.putBoolean("swipe_reaction_checkbox", isSwipable());
        settings.putBoolean("tilt_reaction_checkbox", isTiltable());
        settings.putBoolean("premium", isPremium());
        settings.flush();
        Gdx.app.log("PREF_MANAGER", "Saved changes to filesystem");
        logContent();
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
        writeToFile();
    }
}
