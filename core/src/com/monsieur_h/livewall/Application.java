package com.monsieur_h.livewall;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;

/**
 * @author hubert
 *         Main application for the wallpaper service
 */
public class Application extends ApplicationAdapter {
    private static final String CONFIG_FILE_NAME = "config.json";
    private static final String DEBUG_FILE_NAME = "debug.json";
    private final String TAG = getClass().getSimpleName();
    ArrayList<SceneTemplate> knownSceneTemplates;
    private WallpaperScene stage;
    private SceneTemplate template;
    private boolean debug = false;

    @Override
    public void resize(int width, int height) {
        stage.resize(width, height);
    }

    @Override
    public void create() {
        load();
        startMainScene();
    }

    protected void reload() {
        if (hasSceneChanged()) {
            load();
        }
        startMainScene();
    }

    private void load() {
        if (!isConfigFilePresent()) {
            Gdx.app.error(TAG, "Config file not found, exiting...");
            Gdx.app.exit();
        }

        loadConfig();
        loadUserPrefs();

        if (isDebugFilePresent()) {
            loadDebugConfig();
        }

        if (template == null) {
            Gdx.app.error(TAG, "Could not determine scene template, exiting...");
            Gdx.app.exit();
        }


    }

    private void startMainScene() {
        stage = new WallpaperScene(template);
    }

    private void loadConfig() {
        FileHandle configFile = Gdx.files.internal(CONFIG_FILE_NAME);
        JsonReader jReader = new JsonReader();
        JsonValue rootNode = jReader.parse(configFile);
        knownSceneTemplates = new ArrayList<SceneTemplate>();
        if (rootNode.child != null) {
            for (int i = 0; rootNode.get(i) != null; i++) {
                SceneTemplate newSceneTemplate = new SceneTemplate(rootNode.get(i));
                knownSceneTemplates.add(newSceneTemplate);
            }
        }
    }

    private void loadUserPrefs() {
        String sceneName = PreferenceManager.getInstance().getChosenScene();
        template = getSceneTemplateByName(sceneName); //Or preference name
    }

    private void loadDebugConfig() {
        JsonReader jReader = new JsonReader();
        JsonValue rootNode = jReader.parse(Gdx.files.internal(DEBUG_FILE_NAME));
        if (rootNode.has("debug") && rootNode.getInt("debug") == 1) {
            debug = true;
            PreferenceManager.getInstance().setPremium(true);
        }


        if (debug && rootNode.has("scene")) {
            template = getSceneTemplateByName(rootNode.getString("scene"));
        }
    }

    private SceneTemplate getSceneTemplateByName(String name) {
        for (SceneTemplate knownSceneTemplate : knownSceneTemplates) {
            if (knownSceneTemplate.getName().equals(name)) {
                return knownSceneTemplate;
            }
        }
        return null;
    }

    @Override
    public void render() {
        stage.act(Gdx.graphics.getDeltaTime());
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    private boolean hasSceneChanged() {
        return (!PreferenceManager.getInstance().getChosenScene().equals(template.getName()));
    }

    private boolean isDebugFilePresent() {
        return Gdx.files.internal(DEBUG_FILE_NAME).exists();
    }

    private boolean isConfigFilePresent() {
        return Gdx.files.internal(CONFIG_FILE_NAME).exists();
    }
}
