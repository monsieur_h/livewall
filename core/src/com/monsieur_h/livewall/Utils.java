package com.monsieur_h.livewall;

import java.util.Random;

/**
 * Created by hub on 06/08/15.
 */
public class Utils {
    /**
     * Util : returns a random integer within a range
     *
     * @param min minimum value
     * @param max maximum value
     * @return integer in range
     */
    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}
