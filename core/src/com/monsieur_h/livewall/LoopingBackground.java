package com.monsieur_h.livewall;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

/**
 * Handles a fullscreen background that scales itself correctly
 * The background keeps its aspect ratio and is scaled to fit 100% of the width of the screen
 * It is then scaled up to 'overflow' value to handle transitions (Think Android static backgrounds)
 * Created by hub on 06/08/15.
 */
@SuppressWarnings("FieldCanBeLocal")
class LoopingBackground extends Group {
    private final Image image;
    private final Vector2 screenSize = new Vector2();
    private final float ANIMATION_DURATION = .45f;
    private final float SWIPE_MOVEMENT = 65f; //In percentage of the screen
    private MoveToAction moveAway;

    LoopingBackground(Texture backgroundImage, int width, int height) {
        this.image = new Image(backgroundImage);

        resize(width, height);

        initActions();
        addActor(image);
    }

    private void setBackgroundScale() {
        float scale = screenSize.y / image.getHeight();
        image.setPosition(0, 0);
        image.setScale(scale);
        System.out.println(scale);
    }

    private void initActions() {
        moveAway = new MoveToAction();
        moveAway.setDuration(ANIMATION_DURATION);
        moveAway.setInterpolation(Interpolation.elastic);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        if (image.getX() > 0) {
            image.setX(image.getX() - (image.getWidth() * image.getScaleX()));
        }

        if (image.getX() + (image.getWidth() * image.getScaleX()) < 0) {
            image.setX(image.getX() + (image.getWidth() * image.getScaleX()));
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        float originX = image.getX();
        image.setX(image.getX() - (image.getWidth() * image.getScaleX()));
        image.draw(batch, parentAlpha);

        float x = image.getX() + (image.getWidth() * image.getScaleX());
        while (x < screenSize.x) {
            image.setX(image.getX() + (image.getWidth() * image.getScaleX()));
            image.draw(batch, parentAlpha);
            x = image.getX() + (image.getWidth() * image.getScaleX());
        }

        image.setX(originX);
    }

    void onSwipe(Vector2 direction) {
        if (Math.abs(direction.x) < Math.abs(direction.y))
            return;
        float movement = screenSize.x * SWIPE_MOVEMENT / 100f;
        movement *= direction.x;
        moveAway.setX(image.getX() + movement);
        image.clearActions();
        moveAway.reset();
        image.addAction(moveAway);
    }

    void resize(int w, int h) {
        screenSize.set(w, h);
        setBackgroundScale();
    }
}
