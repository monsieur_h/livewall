package com.monsieur_h.livewall;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hubert
 */
@SuppressWarnings("FieldCanBeLocal")
class WallpaperScene extends Stage implements InputProcessor {
    private final String TAG = getClass().getSimpleName();
    private final float FRIEND_TILT_MAX_DELAY = 0.5f;//In seconds
    private final float ACCELERATION_THRESHOLD = 0.75f; //Acceleration must be X% of MAX's acceleration
    private final String SKELETON_FILENAME = "skeleton.json";
    private final String ATLAS_FILENAME = "skeleton.atlas";
    private final List<PassiveAnimatedCharacter> friends = new ArrayList<PassiveAnimatedCharacter>();
    private final List<TouchInput> touchesList = new ArrayList<TouchInput>();
    private final Accelerometer accelerometer = new Accelerometer();
    private final PolygonSpriteBatch batch = new PolygonSpriteBatch();
    private final AssetManager assetManager = new AssetManager();
    private final SceneTemplate template;
    private AnimatedCharacter hero;
    private LoopingBackground background;

    WallpaperScene(SceneTemplate template) {
        this.template = template;
        setViewport(new ScreenViewport());
        Gdx.input.setInputProcessor(this);

        loadAssets();
        createCharacters();
        placeCharacters();
        resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    private void createCharacters() {
        float characterScale = 0.5f;
        hero = new AnimatedCharacter(characterScale, SKELETON_FILENAME, getAssetManager().get(ATLAS_FILENAME, TextureAtlas.class));
        hero.setSkinByName(template.getHeroName());

        background = new LoopingBackground(assetManager.get(template.getBackgoundName(), Texture.class), getViewport().getScreenWidth(), getViewport().getScreenHeight());
        addActor(background);

        for (int i = 0; i < template.getBuddiesNames().size(); i++) {
            addFriend(template.getBuddiesNames().get(i), hero.getScale());
        }

    }

    private void placeCharacters() {
        Gdx.app.log(TAG, "Placing characters...");
        //Calculating scale...
        float width = Gdx.graphics.getWidth();
        float height = Gdx.graphics.getHeight();


        //Centering the character
        hero.setPosition(width / 2f, height / 3f);


        //Background


        float offset = width / (friends.size() + 1);
        for (int i = 0; i < friends.size(); i++) {
            float x = offset * i;
            x += width / 3;
            placeFriend(friends.get(i), hero.getScale(), x);
        }
        addActor(hero);
    }

    private void addFriend(String name, float baseScale) {
        float randomScale = 0.65f * baseScale;
        PassiveAnimatedCharacter buddy = new PassiveAnimatedCharacter(randomScale, SKELETON_FILENAME, assetManager.get(ATLAS_FILENAME, TextureAtlas.class));

        buddy.setSkinByName(name);
        friends.add(buddy);
        addActor(buddy);
    }

    private void placeFriend(PassiveAnimatedCharacter friend, float baseScale, float xPos) {
        float randomScale = 0.65f * baseScale;
        float y = hero.getPosition().y + randomScale;
        friend.setPosition(xPos, y);

    }

    private void loadAssets() {
        Gdx.app.log(TAG, "Loading assets...");
        assetManager.load(ATLAS_FILENAME, TextureAtlas.class);
        assetManager.load(template.getBackgoundName(), Texture.class);
        assetManager.finishLoading();
    }

    private AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        hero.dispose();
        for (PassiveAnimatedCharacter friend : friends) {
            friend.dispose();
        }
        assetManager.dispose();
    }

    public void act(float delta) {
        super.act(delta);
        checkAccelerometer(delta);
    }

    /**
     * @param delta time elapsed in second since last frame
     */
    private void checkAccelerometer(float delta) {
        accelerometer.update(delta);

        if (accelerometer.getCurrentAcceleration().equals(Vector3.Zero))//No movement
            return;

        float ratioToMax = accelerometer.getAccelerationStrength();
        if (ratioToMax > ACCELERATION_THRESHOLD) {
            Vector3 direction = accelerometer.getDirection();
            hero.onTilt(direction);

            //Make friends react to tilt with a random delay
            for (PassiveAnimatedCharacter friend : friends) {
                float time = (float) (Math.random() * FRIEND_TILT_MAX_DELAY);//0 to 1 second?
                Timer.schedule(new FallingTask(friend, direction), time);
            }
        }
    }


    /**
     * Callback method to make a friend fall
     */
//    public void friendSchedulerCallback(Vector3 direction) {
//        this.friends.get(this.friendIndex).onTilt(direction);
//    }
    public void draw() {
        batch.setProjectionMatrix(getCamera().combined);
        batch.begin();
        getRoot().draw(batch, 1);
        batch.end();
    }

    /**
     * Notifies the hero of a touch event
     *
     * @param touchIndex the index of the touch
     */
    private void notifyHero(int touchIndex) {
        TouchInput currentTouch = getTouchByIndex(touchIndex);
        if (currentTouch != null) {
            Vector2 copy = currentTouch.getTouchUp().cpy();
            Vector3 screenSpaceCoordinates = new Vector3(copy.x, copy.y, 0);
            Vector3 worldSpaceCoordinates = getCamera().unproject(screenSpaceCoordinates);

            //HACK: 200 seems to be arbitrary good
            //TODO: make it resolution-relative or tweakable by settings
            if (currentTouch.getMovementDistance() > 200f) {
                hero.onSwipe(currentTouch.getDirection());
                for (PassiveAnimatedCharacter cha : friends)
                    cha.onSwipe(currentTouch.getDirection());

            } else {
                if (hero.contains(worldSpaceCoordinates.x, worldSpaceCoordinates.y)) {
                    hero.onTapOn();
                } else {
                    hero.onTapOut(worldSpaceCoordinates.x, worldSpaceCoordinates.y);
                }
            }
        }
    }

    /**
     * Makes the background react to a touch
     * TODO: Refactor all 'notify' elements with an observer pattern
     *
     * @param touchIndex index of the pointer
     */
    private void notifyBackground(int touchIndex) {
        TouchInput currentTouch = getTouchByIndex(touchIndex);
        if (currentTouch != null) {
            //HACK: 200 seems to be arbitrary good
            //TODO: make it resolution-relative or tweakable by settings
            if (currentTouch.getMovementDistance() > 200f) {
                background.onSwipe(currentTouch.getDirection());
            }
        }
    }

    public boolean touchUp(int x, int y, int pointer, int button) {
        notifyTouch(x, y, pointer, true);
        notifyHero(pointer);
        notifyBackground(pointer);
        return false;
    }

    public boolean touchDown(int x, int y, int pointer, int button) {
        notifyTouch(x, y, pointer, false);
        return false;
    }

    /**
     * Registers a touch event
     *
     * @param x       coord
     * @param y       coord
     * @param pointer index of the touch (0 being the first finger etc)
     * @param up      is it touchup or touchdown?
     */
    private void notifyTouch(int x, int y, int pointer, boolean up) {
        TouchInput currentTouch = getTouchByIndex(pointer);
        if (currentTouch == null) {
            currentTouch = new TouchInput(pointer);
            touchesList.add(currentTouch);
        }
        if (up) {
            currentTouch.onTouchUp(x, y);
        } else {
            currentTouch.onTouchDown(x, y);
        }
    }

    private TouchInput getTouchByIndex(int index) {
        for (TouchInput touch : touchesList) {
            if (touch.getIndex() == index) {
                return touch;
            }
        }
        return null;
    }

    void resize(int width, int height) {
        Gdx.app.log("TEST", String.format("resizing (%dx%d)...", width, height));
        getViewport().update(width, height, true);
        background.resize(getViewport().getScreenWidth(), getViewport().getScreenHeight());
        placeCharacters();
    }
}
