package com.monsieur_h.livewall;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

public class PassiveAnimatedCharacter extends AnimatedCharacter {
    private static float DEFAULT_MOVING_PROBABILITY = 25f;
    private static float MOVING_SPEED = 2.5f;
    private static float MOVING_MAX_LOOP = 3;

    private Vector2 direction;
    private int loops;
    private float movingProbability;

    public PassiveAnimatedCharacter(float scale, String skeletonFileName, TextureAtlas atlas) {
        super(scale, skeletonFileName, atlas);
    }

    protected void init(float scale, String skeletonFileName, TextureAtlas atlas) {
        super.init(scale, skeletonFileName, atlas);
        this.state.setTimeScale(1.5f - this.getScale());
        this.movingProbability = DEFAULT_MOVING_PROBABILITY;
    }

    public void setMovingProbability(float movingProbability) {
        this.movingProbability = movingProbability;
    }

    public void setNewIdleAnimation() {
        super.setNewIdleAnimation();
        float random = (float) Math.random() * 100;
        if (random < movingProbability) {
            this.initMove();
        }
    }

    public void initMove() {
        float xDirection;
        if (this.getX() > this.getStage().getWidth() || this.getX() < 0) {
            float stageCenterX = this.getStage().getWidth() / 2;
            float position = getX() - stageCenterX;
            xDirection = position * -1;
        } else {
            xDirection = (float) Math.random() - 0.5f;
        }
        this.direction = new Vector2(xDirection, 0).nor();
        this.skeleton.setFlipX((xDirection > 0));
        this.loops = (int) (Math.random() * (MOVING_MAX_LOOP - 1)) + 1;//1 to MOVING_MAX_LOOP
        this.state.setAnimation(0, "move", true);
    }

    public void act(float delta) {
        super.act(delta);
        String currentAnimationName = this.state.getCurrent(0).getAnimation().getName();

        if (currentAnimationName.equalsIgnoreCase("move")) {
            Vector2 move = this.direction.cpy();
            move.scl(this.getScale());
            move.scl(MOVING_SPEED);
            move.add(this.position);
            this.setPosition(move.x, move.y);
        }
    }

    public void complete(int trackIndex, int loopCount) {
        super.complete(trackIndex, loopCount);

        String currentAnimationName = this.state.getCurrent(0).getAnimation().getName();
        if (currentAnimationName.equals(IDLE_ANIMATION_NAME)) {
            if (loopCount > MAX_IDLE_LOOP) {
                setNewIdleAnimation();
            }
        } else if (loopCount > this.loops) {
            this.skeleton.setFlipX(false);
            this.setNewIdleAnimation();
        }
    }
}

