package com.monsieur_h.livewall;

import com.badlogic.gdx.utils.JsonValue;

import java.util.ArrayList;

/**
 * A scene template is a set of values defining a specific Scene
 * Created by hubert on 30/07/15.
 */
public class SceneTemplate {
    private final String name;
    private final String heroName;
    private final String backgoundName;
    private final ArrayList<String> buddiesNames;
    private final boolean free;
    private JsonValue node;
    public SceneTemplate(JsonValue node) {
        this.node = node;
        this.name = node.getString("name");
        this.heroName = node.getString("hero");
        this.backgoundName = node.getString("background");
        this.buddiesNames = new ArrayList<String>();
        JsonValue buddies = node.get("buddies");
        for (int i = 0; buddies.get(i) != null; i++) {
            this.buddiesNames.add(buddies.getString(i));
        }
        this.free = node.getBoolean("free", false);
    }

    public boolean isFree() {
        return free;
    }

    public String getName() {
        return name;
    }

    public String getHeroName() {
        return heroName;
    }

    public String getBackgoundName() {
        return backgoundName;
    }

    public ArrayList<String> getBuddiesNames() {
        return buddiesNames;
    }
}
