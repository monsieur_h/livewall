package com.monsieur_h.livewall;

import com.badlogic.gdx.math.Vector2;

/**
 * @author hubert
 * Helper class to store touch coordinates and events
 */
public class TouchInput {
	private Vector2 touchDown;
	private Vector2 touchUp;
	private int touchIndex;
	
	public TouchInput(int index){
		init(index, 0);
	}
	
	public TouchInput(int index, int button){
		init(index, button);		
	}
	
	private void init(int index, int button){
		this.touchDown = new Vector2();
		this.touchUp = new Vector2();
	}
	
	public void onTouchDown(int x, int y){
		this.touchDown.x = x;
		this.touchDown.y = y;
	}
	
	public void onTouchUp(int x, int y){
		this.touchUp.x = x;
		this.touchUp.y = y;
	}
	
	public int getIndex(){
		return this.touchIndex;
	}
	
	public Vector2 getMovement(){
		Vector2 touchCopy = this.touchUp.cpy();
		return touchCopy.sub(this.touchDown);
	}
	
	public Vector2 getDirection(){
		Vector2 d = this.touchUp.cpy().sub(this.touchDown);
		d.nor();
		return d;
	}
	
	public Vector2 getTouchUp(){
		return this.touchUp;
	}
	
	public Vector2 getTouchDown(){
		return this.touchDown;
	}
	
	public float getMovementDistance(){
		return this.getMovement().len();
	}
	
	public String toString(){
		return this.getTouchDown().toString() + " " + this.getTouchUp().toString();
	}
}
