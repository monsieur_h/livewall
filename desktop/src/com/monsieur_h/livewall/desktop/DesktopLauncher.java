package com.monsieur_h.livewall.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.monsieur_h.livewall.Application;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 720/2;
		config.height = 1280/2;
		new LwjglApplication(new Application(), config);//Use this to launch the wallpaper
//		new LwjglApplication(new PurchasePreviewApplication(), config);//Use this to launch the purchase/preview screen
	}
}
