# Home Menu
![home](home_menu.png)

- When I tap on the screen on the main character, it reacts
- When I tap on the screen outside of the main character, it reacts (left and right)
- When I swipe left/right/or vertically, all the character react
- When I shake my device all the characters react
- When I click on the app Icon, the [Wallpaper Settings Activity](#Wallpaper-Settings-Activity) opens



# Wallpaper Settings Activity
![settings](wp_settings.png)

- When I select the wallpaper in the list, I can then press the "select" button to go back to the home menu. The wallpaper is now present (see [Home Menu section][#Home-Menu])
- When I click the settings button, the [Settings Menu](#Settings-Menu) opens



# Settings Menu
![settings](settings.png)

- When I disable swipe/tap/tilt the according behavior are disabled in the Home Menu
- When I press the "Change skin" button, the [Skin picker menu](#Skin-picker-menu) screen opens
- When I press the "Go Premium" button, I'm invited through a Android purchase process to go premium
- Other applications should open the Android Market
- the "About" button must show a screen giving contact infos

# Skin picker menu
![skin](skin_pick.png)

- I can browse the skins by swiping
- The available skins propose a "Select" button
- The currently selected skin propose a "Back" button
- The locked skins show a banner and a "Key" button
