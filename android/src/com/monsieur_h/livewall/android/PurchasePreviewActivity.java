package com.monsieur_h.livewall.android;

import android.os.Bundle;
import android.util.Log;
import android.content.Context;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.monsieur_h.livewall.PurchasePreviewApplication;
import com.monsieur_h.util.IabHelper;
import com.monsieur_h.util.IabResult;

public class PurchasePreviewActivity extends AndroidApplication{
	
	private PurchasePreviewApplication app;
	
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		this.app = new PurchasePreviewApplication();

		initialize(this.app, cfg);
	}
	
	

	@Override
	public void onDestroy(){
		super.onDestroy();
	}
}
