package com.monsieur_h.livewall.android;

import android.util.Log;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;
import com.badlogic.gdx.backends.android.AndroidWallpaperListener;
import com.monsieur_h.livewall.Application;

public class AndroidLauncher extends AndroidLiveWallpaperService {
    @Override
    public void onCreate() {
        super.onCreate();
    }

    public ApplicationListener createListener() {
        return new MyLiveWallpaperListener();
    }

    public AndroidApplicationConfiguration createConfig() {
        return new AndroidApplicationConfiguration();
    }

    public void onCreateApplication() {
        super.onCreateApplication();

        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.getTouchEventsForLiveWallpaper = true;
        config.numSamples = 4;

        ApplicationListener listener = new MyLiveWallpaperListener();
        initialize(listener, config);
    }

    private static class MyLiveWallpaperListener extends Application implements AndroidWallpaperListener {
        @Override
        public void offsetChange(float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset,
                                 int yPixelOffset) {
            Log.i("OFFSET", "offsetChange(xOffset:" + xOffset + " yOffset:" + yOffset + " xOffsetSteep:" + xOffsetStep + " yOffsetStep:" + yOffsetStep + " xPixelOffset:" + xPixelOffset + " yPixelOffset:" + yPixelOffset + ")");
        }

        @Override
        public void previewStateChange(boolean isPreview) {
            super.reload();
            Log.i("PREVIEW", "previewStateChange(isPreview:" + isPreview + ")");
        }

        @Override
        public void resize(int width, int height) {
            super.resize(width, height);
            Log.i("RESIZE", "W:" + width + " H:" + height);
        }
    }
}