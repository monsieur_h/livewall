package com.monsieur_h.livewall.android;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.*;
import android.preference.Preference.OnPreferenceClickListener;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Peripheral;
import com.monsieur_h.util.IabHelper;
import com.monsieur_h.util.IabResult;
import com.monsieur_h.util.Inventory;
import com.monsieur_h.util.Purchase;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p/>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
    /**
     * Determines whether to always show the simplified settings UI, where
     * settings are presented in a single list. When false, settings are shown
     * as a master/detail two-pane view on tablets. When true, a single pane is
     * shown on tablets.
     */
    private static final boolean ALWAYS_SIMPLE_PREFS = false;
    private static final String SKU_PREMIUM = "premium_pass";
    private static final String SHARED_PREFERENCE_NAME = "com.monsieur_h.livewall";
    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener sBindPreferenceSummaryToValueListener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {

            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                // For list preferences, look up the correct display value in
                // the preference's 'entries' list.
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                // Set the summary to reflect the new value.
                preference
                        .setSummary(index >= 0 ? listPreference.getEntries()[index]
                                : null);
            } else {
                // For all other preferences, set the summary to the value's
                // simple string representation.
                preference.setSummary(stringValue);
            }
            return true;
        }
    };
    private final String publicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA3xUb9Uy9c/y2559JJpp+h2JjgEEFA9Qq4JeoQkne5QbFyX1pxdEsUs++nRT+uwEn/aKduNgeMLozb+Gaefs8N4qAMBN342hCLNtMZXI1ycDaFebAvTjkEWv0drsoqgo8H9hJbKiX9eWu8AyYX40b76laTAF9gpVw8vOrl8slIYJWBFSAcgFGEETcAWFfcdMkgRIECc1lmmKfURoGjtFJEHXQrs9do1Gy1R5LtPK6bvklSg1UsmdP6UIjbvAc0c+RuPmuAMysyo5pOdh/5/kv3xNESxCFN/RHfz+ZymOYWcFJ1MO+YHjujV0AGc9wmSP9fWcmpIWLiGIFQCe/09nOOwIDAQAB";
    public IabHelper mHelper;
    //Listener for purchase completion
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener
            = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("IAB", "Purchase finished callback");
            if (result.isFailure()
                    && result.getResponse() != IabHelper.BILLING_RESPONSE_RESULT_OK
                    && result.getResponse() != IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED
                    && result.getResponse() != IabHelper.IABHELPER_VERIFICATION_FAILED) {
                Log.d("IAB", "Error purchasing: " + result);
                showErrorMsg(getString(R.string.error_template) + result.getMessage());
                com.monsieur_h.livewall.PreferenceManager.getInstance().setPremium(false);
            } else {
                Log.d("IAB", "Activating premium : Everything is ok");
                com.monsieur_h.livewall.PreferenceManager.getInstance().setPremium(true);
                showPremiumPurchased();
            }
        }
    };
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener
            = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result,
                                             Inventory inventory) {

            if (result.isFailure()) {
                Log.e("IAB", "Could not fetch purchased products from google server" + result.toString());
            } else {
                Log.d("IAB", "Inventory successfully fetched" + result.toString());
                boolean isPremium = inventory.hasPurchase(SKU_PREMIUM);
                SettingsActivity.this.setPremium(isPremium);
                if (isPremium)
                    Log.d("IAB", "User is premium!");
                else
                    Log.d("IAB", "User is not premium");
            }
        }
    };

    /**
     * Helper method to determine if the device has an extra-large screen. For
     * example, 10" tablets are extra-large.
     */
    private static boolean isXLargeTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
    }

    /**
     * Determines whether the simplified settings UI should be shown. This is
     * true if this is forced via {@link #ALWAYS_SIMPLE_PREFS}, or the device
     * doesn't have newer APIs like {@link PreferenceFragment}, or the device
     * doesn't have an extra-large screen. In these cases, a single-pane
     * "simplified" settings UI should be shown.
     */
    private static boolean isSimplePreferences(Context context) {
        return ALWAYS_SIMPLE_PREFS
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB
                || !isXLargeTablet(context);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("IAB", "Setting up IAB...");
        this.mHelper = new IabHelper(this, this.publicKey);
        this.mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.d("IAB", "There was an error connecting to the purchase server...");
                } else {
                    Log.d("IAB", "IAB successfully initialized");
                    if (SettingsActivity.this.mHelper.mAsyncInProgress) {
                        SettingsActivity.this.showErrorMsg("A purchase operation is already in progress");
                    } else {
                        SettingsActivity.this.mHelper.queryInventoryAsync(mGotInventoryListener);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (this.mHelper != null)
            this.mHelper.dispose();
        this.mHelper = null;
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        PreferenceManager prefMgr = getPreferenceManager();
        prefMgr.setSharedPreferencesName(SHARED_PREFERENCE_NAME);
        prefMgr.setSharedPreferencesMode(MODE_WORLD_READABLE);
        super.onPostCreate(savedInstanceState);

        setupSimplePreferencesScreen();
    }

    /**
     * Shows the simplified settings UI if the device configuration if the
     * device configuration dictates that a simplified, single-pane UI should be
     * shown.
     */
    @SuppressWarnings("deprecation")
    private void setupSimplePreferencesScreen() {
        if (!isSimplePreferences(this)) {
            return;
        }

        // In the simplified UI, fragments are not used at all and we instead
        // use the older PreferenceActivity APIs.

        // Add 'general' preferences.
        addPreferencesFromResource(R.xml.pref_general);


        //Adding "Customize" header
        PreferenceCategory customizeHeader = new PreferenceCategory(this);
        customizeHeader.setTitle(R.string.customize);
        getPreferenceScreen().addPreference(customizeHeader);
        //Adding customize prefs
        addPreferencesFromResource(R.xml.pref_customize);

        Preference chooseSkin = findPreference("skin");
        chooseSkin.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Context context = getApplicationContext();
                CharSequence text = getString(R.string.swipe_advice);
                int duration = Toast.LENGTH_SHORT;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();


                Intent i = new Intent(getBaseContext(), PurchasePreviewActivity.class);
                startActivity(i);
                return false;
            }
        });

        //Adding "GO PREMIUM"
        Preference goPremium = findPreference("purchase_skin");
        goPremium.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (SettingsActivity.this.mHelper == null) {
                    Log.e("IAB", "Could not launch purchase flow, IAB helper not set");
                    return false;
                }
                if (SettingsActivity.this.mHelper.mAsyncInProgress) {
                    SettingsActivity.this.showErrorMsg("A purchase operation is already in progress");
                } else {
                    SettingsActivity.this.mHelper.launchPurchaseFlow(SettingsActivity.this,
                            SKU_PREMIUM,
                            10001,
                            mPurchaseFinishedListener,
                            "");
                }
                return false;
            }
        });

        //Add a header
        PreferenceCategory aboutHeader = new PreferenceCategory(this);
        aboutHeader.setTitle(R.string.about);
        getPreferenceScreen().addPreference(aboutHeader);
        //Load about prefs
        addPreferencesFromResource(R.xml.pref_about);
        //Add see apps button
        Preference pref = findPreference("market");
        pref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference arg0) {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://search?q=pub:monsieur_h")));
                return false;
            }
        });
        getPreferenceScreen().addPreference(pref);

        //Add button for dialog
        Preference shower = findPreference("about");
        shower.setOnPreferenceClickListener(new OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                showAbout();
                return false;
            }
        });
        getPreferenceScreen().addPreference(shower);

        //Check what prefs to disables (based on purchases and/or accelerometer availability)
        boolean accelerometerAvailable = Gdx.input.isPeripheralAvailable(Peripheral.Accelerometer);
        if (!accelerometerAvailable) {
            getPreferenceScreen().findPreference("tilt_reaction_checkbox").setEnabled(false);
            getPreferenceScreen().findPreference("tilt_reaction_checkbox").setSummary(R.string.accelerometer_disabled);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("IAB", "onActivityResult(" + requestCode + "," + resultCode + "," + data);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Log.d("IAB", "onActivityResult handled by IABUtil.");
        }
    }

    private void setPremium(boolean isPremium) {
        SharedPreferences pref = getSharedPreferences(SHARED_PREFERENCE_NAME, MODE_PRIVATE);
        Editor e = pref.edit();
        e.putBoolean("premium", isPremium);
        e.commit();
    }

    protected void showAbout() {
        // Inflate the about message contents
        View messageView = getLayoutInflater().inflate(R.layout.activity_about, null, true);

        // When linking text, force to always use default color. This works
        // around a pressed color state bug.
//        TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
//        int defaultColor = textView.getTextColors().getDefaultColor();
//        textView.setTextColor(defaultColor);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.ic_launcher);
        builder.setTitle(R.string.app_name);
        builder.setMessage(R.string.about_us);
        builder.setView(messageView);
        builder.create();
        builder.show();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean onIsMultiPane() {
        return isXLargeTablet(this) && !isSimplePreferences(this);
    }

    private void showErrorMsg(String msg) {
        AlertDialog alertDialog = new AlertDialog.Builder(SettingsActivity.this).create(); //Read Update
        alertDialog.setTitle("Error");
        alertDialog.setMessage(msg);

        alertDialog.setButton("Continue..", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        alertDialog.show();
    }

    private void showPremiumPurchased() {
        AlertDialog alertDialog = new AlertDialog.Builder(SettingsActivity.this).create(); //Read Update
        alertDialog.setTitle(R.string.thank_you);
        String thankYouMessage = getApplicationContext().getString(R.string.purchase_ok);
        alertDialog.setMessage(thankYouMessage);


        alertDialog.setButton("Continue..", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    /**
     * This fragment shows general preferences only. It is used when the
     * activity is showing a two-pane settings UI.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public static class GeneralPreferenceFragment extends PreferenceFragment {
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.pref_general);

            // Bind the summaries of EditText/List/Dialog/Ringtone preferences
            // to their values. When their values change, their summaries are
            // updated to reflect the new value, per the Android Design
            // guidelines.
//			bindPreferenceSummaryToValue(findPreference("example_text"));
//			bindPreferenceSummaryToValue(findPreference("example_list"));
        }
    }
}
