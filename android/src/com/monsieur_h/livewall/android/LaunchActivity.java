package com.monsieur_h.livewall.android;

import android.app.LauncherActivity;
import android.app.WallpaperManager;
import android.content.Intent;
import android.os.Bundle;

/**
 * A simple bootstrap Activity used to pop the Wallpaper Chooser activity.
 * Created by hubert on 21/07/15.
 */
public class LaunchActivity extends LauncherActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
        startActivity(intent);
        finish();
    }
}
