# README

livewall project is a template project to generate Android Live Wallpapers using Spine (Esoteric Software).

## What to do ?

In order to make a full Live Wallpaper, you have to provide an fully animated character and a background scene.

### Character

The character will be featured at the center of the screen. It can be facing the screen or looking sideways. Or both. The animations will be triggered depending on different input sources : 

* A tap on the screen
* A tap on the character
* A lateral swipe (when the user browses his applications pages)
* A tilt of the device

See Animation paragraph for more details.

### Skins and scenes

A scene is composed of one or more characters and a background. A skin may be either free or restricted to premium users. The definition of skins and scenes is made using a config file .

#### Config files

##### config.json
This config files allows you to describe the scenes availables in the application. Characters are referenced using the name of their skins.
The background is simply referenced by the name of its image.

**Note :** Backgrounds will be upscaled to fit the width of the device (potentially hiding the top and the bottom of the image). You may want to adapt the ratio of your image accordingly.


Sample :
```
{
    "name": "testScene",
    "hero": "potato",
    "background": "background.png",
    "free":true,
    "buddies": [
      "green",
      "green",
      "green"
    ]
  },
  {
    "name": "ElHombre",
    "hero": "sombrero",
    "background": "pampa.png",
    "free":false,
    "buddies": [
    ]
  }
```
The preceding sample describes a file containing two scenes. The first one is free and contains a hero and 3 friends with the same skin.
The second one contains a single hero with a different background and is not free.

##### debug.json
This file is here for development purposes only. It allows the developper to select a default scene even if the scene is a premium scene.

Sample :
```
{
  "debug": 1,
  "scene": "my_scene"
}
```

### Animations

*The Spine project must contain a set of animation with precise names to achieve a correct behavior of the main character.*

#### Animations sets

* "idle*" - every animation which name begins with "idle" belong to the idle animations pool. They will be randomly triggered after every N loops of the "idle" animation. (e.g. "idle-sleep", "idle-run"...)
* "*important" - any animation containing the word "important" will not be interrupted by any action. For example, if your character falls on screen tilt but jumps on tap, you don't want it to go from "falling" to "jumping". So you may want to rename your animation in between "standingup-important", to avoid it being interrupted. When an animation can not be run, its not queued.

#### Special animations

* "idle" - this is the default animation the character will loop on. Every N loops, it will take a new animation from the idle animations pool
* "onTapOut" - triggered when the user taps on the screen outside of the character. The animation must react to a tap event on the right of the screen. If it happens to the left, the animation will simply be flipped. It's usually a very short animation (~0.2s - 0.5s)
* "onTapIn" - triggered when the user taps on the character. Usually a very short animation (~0.2s - 0.5s)
* "onTilt" - triggered when the device is tilted. The character must react to the right of the screen (e.g. fall on the right). The animation will be flipped if the tilt comes from the opposite direction.
